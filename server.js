const express = require("express");
const cors = require("cors");
//const bodyParser = require("body-parser");
const mongoose = require("mongoose");

/*************************
 * MONGO DATABASE
 ************************/

//create new mongodb server
mongoose.connect(process.env.DATABASE_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
const db = mongoose.connection;

// handle requests
db.on("error", error => console.log(error));
db.once("open", () => console.log(">>> Connected to Mongoo DB"));

/*************************
 * EXPRESS SERVER
 ************************/
const app = express();

// enable CORS
const corsOptions = {
  origin: process.env.HOST_URL
};
app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
//app.use(bodyParser.urlencoded({ extended: true }));

/***********************************
 * server routes
 ***********************************/
const usersRouter = require("./back/routes/users");
app.use("/users", usersRouter);

// start server
app.listen(3000, () => console.log(">>> Server started"));
