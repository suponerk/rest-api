### REST api service using MongoDb (mongoose), Express.js and Node.js

This service is written just to learn how to make backend from scratch.

Key features:

- Supports Authorization via name and password.
- Password is encrypted using Bcrypt library.
- CRUD operations (REST service) via JSON

Now it has just 1 entry point: `http://<this_host_url>/users/`

- Routers (controllers) located in `/back/routers`
- Models located in `/back/models`
- Database (mongodb) located in `/back/database`

Look at `/back/routers/users.js` to see all API commands

### How to start service

1. If you are not using glitch.com service, add `dotenv` package to your `package.json`. If you are using npm, type `npm install dotenv`
2. Edit file `.env.example` and type your credentials there. Rename to `.env` after finish
3. Run your db and server. You have 2 options. (or you can use pm2 process manager):
   - run database in separate process `npm run db`, and then run server `npm run start` (preffered)
   - run all in 1 command `npm run start-all`

### How to test:

You can use any type of service, valid to send GET, POST, DELETE and PATCH http requests to this server.

For example, this service [Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) is good enougth.

Each POST request must send `"Content-type": "application/json"` in HTTP Headers to work properly

### Additional info

Database (mongodb) is launched in minimal mode with no logs to minimize memory using due to limitations on cloud service (Glitch.com).

Now each db uses only ~ 20 MB (4 MB for .ns files + 16 MB)

You can delete additional keys to disable such optimizations.
