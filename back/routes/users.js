const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");

const User = require("../models/user");

// login
router.post("/login", async (req, res) => {
  try {
    const user = await User.findOne({ name: req.body.name });
    //console.log(user);
    if (user === []) return res.status(400).send("This user does not exists");
    const passwordMatch = await bcrypt.compare(
      req.body.password,
      user.password
    );
    //console.log("passwordMatch", passwordMatch);
    if (passwordMatch) return res.send("Success");
    return res.send("Not Allowed");
  } catch (err) {
    res.status(500).send();
  }
});

// Getting all
router.get("/", async (req, res) => {
  //res.send("users router");
  try {
    const users = await User.find();
    const prettyUsers = users.map(user => prettifyUser(user));
    res.json({ users: prettyUsers });
  } catch (err) {
    res.status(500).json({ message: "Internal server error" });
  }
});

// Getting one
router.get("/:id", async (req, res) => {
  try {
    const foundUser = await User.findById(req.params.id);
    const user = prettifyUser(foundUser);
    res.json({ user });
  } catch (err) {
    res.status(500).json({ message: "User does not exists" });
  }
});

// Creating one
router.post("/", async (req, res) => {
  let userExists = await User.exists({ name: req.body.name });
  if (userExists) return res.status(500).send("User exists");
  //const salt = await bcrypt.genSalt(); // default 10
  //const hashedPassword = await bcrypt.hash(req.body.password, salt);
  const hashedPassword = await bcrypt.hash(req.body.password, 10); //salt added automatically
  const user = new User({
    name: req.body.name,
    password: hashedPassword
  });
  try {
    const newUser = await user.save();
    res.status(201).json({ message: "User created", newUser });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Updating one
router.patch("/:id", async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    if (user == null) {
      return res.status(400).json({ message: "User does not exists" });
    }
    if (req.body.name != null) user.name = req.body.name;
    if (req.body.password != null) user.password = req.body.password;
    const updatedUser = await user.save();
    res.json({ message: "User updated", updatedUser });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Deleting one
router.delete("/:id", async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    if (user == null) {
      return res.status(400).json({ message: "User does not exists" });
    }
    const deletedUser = await user.remove();
    res.json({ message: "User removed", deletedUser });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

function prettifyUser(user) {
  return {
    id: user._id,
    name: user.name,
    password: user.password
  };
}

module.exports = router;
